const {Servicio} = require('../Services')

async function Adaptador({id}){
    try {
        let {statusCode,data,message} = await Servicio({id})

    return {statusCode, data, message}
    } catch (error) {
        console.log({step:'adapter Adaptador', error: error.toString()})

        return {statusCode:500, message: error.toString()}
    }
    
}

module.exports = {Adaptador}

