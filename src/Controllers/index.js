const {db} = require('../Models')
const {setTimeout}  = require('timers/promises')

async function existUser({id}){
    try {

        const match = db.some(el =>el.id===id)
            
        return {statusCode:200, data: match}
        

    } catch (error) {
         console.log({step:'controller ExisdUser', error: error.toString()})

        return {statusCode:500, message: error.toString()}
    }
   
}

async function findUser({id}){
    try {


        const user = db.filter(el =>el.id===id)[0]
        console.log(user)

        await setTimeout(5000)

        return {statusCode:200, data: user}

    } catch (error) {
         console.log({step:'controller findUser', error: error.toString()})

        return {statusCode:500, message: error.toString()}
    }
   
}



module.exports ={findUser,existUser}



