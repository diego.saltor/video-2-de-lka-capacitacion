const {findUser, existUser} = require('../Controllers')

async function Servicio({id}){
    try {

        const exituser = await existUser({id})

        if(exituser.statusCode!==200)throw(exituser.message)

        if(!exituser.data)throw("no existe el usuario")

        const finduser = await findUser({id})

        if(finduser.statusCode!==200)throw(findUser.message)

        if(finduser.data.info.edad >18){
             console.log("eres mayor de edad")
        }

        return {statusCode:200, data:finduser.data}
        
 
    } catch (error) {
        console.log({step:'service Servicio', error: error.toString()})

        return {statusCode:500, message: error.toString()}
    }
    

    
}

module.exports = {Servicio}
